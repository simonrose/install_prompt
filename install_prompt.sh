#!/bin/bash

srcdir=`pwd`

pushd $HOME

if [ ! -d .bash-git-prompt ]; then
    echo "Bash git prompt not installed. Installing..."
    git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1

    cp $srcdir/.git-prompt-colors.sh .

cat <<EOF >> .bashrc
if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=0
    GIT_PROMPT_THEME=Custom
    source $HOME/.bash-git-prompt/gitprompt.sh
fi
EOF

fi

echo "[alias]" >> .gitconfig
echo -e "\tg = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all" >> .gitconfig

popd
